import {  HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { CognitoService } from "../user-login/service/cognitoService.service";


@Injectable()
export class JWTInterceptor implements HttpInterceptor{

    constructor(private authenticationService :CognitoService){}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const isLoggedIn = this.authenticationService.isAllAuthenticated();
        const isApiUrl = request.url.startsWith(environment.baseUrl);
        console.log("interceptior")

        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.authenticationService.getAutorizationToken()}`
                }
            });
        }
        return next.handle(request)    
    }

}