import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PersonaComponent } from './persona.component';
import { PersonaRoutingModule } from './persona-routing.module';
import {TableModule} from 'primeng/table';
import {ToastModule} from 'primeng/toast';
import {CalendarModule} from 'primeng/calendar';
import {SliderModule} from 'primeng/slider';
import {MultiSelectModule} from 'primeng/multiselect';
import {ContextMenuModule} from 'primeng/contextmenu';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {DropdownModule} from 'primeng/dropdown';
import {ProgressBarModule} from 'primeng/progressbar';
import {InputTextModule} from 'primeng/inputtext';
import {FileUploadModule} from 'primeng/fileupload';
import {ToolbarModule} from 'primeng/toolbar';
import {RatingModule} from 'primeng/rating';
import {RadioButtonModule} from 'primeng/radiobutton';
import {InputNumberModule} from 'primeng/inputnumber';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { ProductService } from '../service/productservice';
import { FormsModule } from '@angular/forms';
import { PersonaService } from '../service/persona.service';


@NgModule({
  declarations: [
    PersonaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,    
    DropdownModule,
    ButtonModule,
    DialogModule,
    ContextMenuModule,
    TableModule,
    ToastModule,
    SliderModule,
    InputTextareaModule,
    RadioButtonModule,
    RatingModule,
    ToolbarModule,
    FileUploadModule,
    InputTextModule,
    ProgressBarModule,
    InputNumberModule,
    CalendarModule,
    MultiSelectModule,
    ConfirmDialogModule,
    PersonaRoutingModule,
    
  ],
  providers:[ProductService,PersonaService, MessageService, ConfirmationService]
})
export class PersonaModule { }
