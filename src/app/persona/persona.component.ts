import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/productservice';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
//import {InputNumberModule} from 'primeng/inputnumber';
import { Persona } from '../domain/persona';
import { PersonaService } from '../service/persona.service';
@Component({
  selector: 'app-persona',
  templateUrl: './persona.component.html',
  styleUrls: ['./persona.component.css']
})
export class PersonaComponent implements OnInit {
    productDialog: boolean=false;

    //products: Product[]=[];

    //product: Product={};
    persona: Persona={};
    personas: Persona[]=[];

    selectedPersonas: Persona[]=[];

    submitted: boolean=false;

    constructor(private productService: ProductService,private personaService:PersonaService, private messageService: MessageService, private confirmationService: ConfirmationService) { }

    ngOnInit() {
        //this.productService.getProducts().then(data => this.products = data);
        this.listarPersona()
    }

    listarPersona(){
        this.personaService.getPersonas().subscribe((data)=>{
            this.personas=data
        },(err)=>{
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
        })
    }
    openNew() {
        this.persona = {};
        this.submitted = false;
        this.productDialog = true;
    }

    deleteSelectedPersonas() {
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete the selected products?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.personas = this.personas.filter(val => !this.selectedPersonas.includes(val));
                this.selectedPersonas = [];
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
            }
        });
    }

    editPersona(persona: Persona) {
        this.persona = {...persona};
        this.productDialog = true;
    }

    deletePersona(persona: Persona) {
       /* this.confirmationService.confirm({
            message: 'Are you sure you want to delete ' + product.name + '?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.products = this.products.filter(val => val.id !== product.id);
                this.product = {};
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
            }
        });*/
    }

    hideDialog() {
        this.productDialog = false;
        this.submitted = false;
    }
    
    savePersona() {
        this.submitted = true;
        if(this.persona.nombre?.trim()){

            if(this.persona.id){
                this.personaService.update(this.persona).subscribe(rep=>{
                    this.persona = {};
                    this.submitted = false;
                    this.productDialog = false;
                    this.messageService.add({severity:'success', summary: 'Successful', detail: 'Persona actualizada', life: 3000});
                    this.listarPersona()
                },(err)=>{
                    
                })
            }else{
                this.personaService.create(this.persona).subscribe(rep=>{
                    this.persona = {};
                    this.submitted = false;
                    this.productDialog = false;
                    this.messageService.add({severity:'success', summary: 'Successful', detail: 'Persona registrada', life: 3000});
                    this.listarPersona()
                },(err)=>{
                    
                })
            }

        }
        /*if (this.product.name?.trim()) {
            if (this.product.id) {
                this.products[this.findIndexById(this.product.id)] = this.product;                
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
            }
            else {
                this.product.id = this.createId();
                this.product.image = 'product-placeholder.svg';
                this.products.push(this.product);
                this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Created', life: 3000});
            }

            this.products = [...this.products];
            this.productDialog = false;
            this.product = {};
        }*/
    }

    findIndexById(id: string): number {
        let index = -1;
        for (let i = 0; i < this.personas.length; i++) {
            if (this.personas[i].id === id) {
                index = i;
                break;
            }
        }

        return index;
    }

    createId(): string {
        let id = '';
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for ( var i = 0; i < 5; i++ ) {
            id += chars.charAt(Math.floor(Math.random() * chars.length));
        }
        return id;
    }
}
