import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from "./app.component";

const routes: Routes = [
    //{ path: "login", component: SignInComponent,canActivate: [IsNotAuthenticated]},
    { path: "", redirectTo: 'persona',pathMatch: 'full',},
    { 
        path: "persona",
        loadChildren:() => import('./persona/persona.module').then(m => m.PersonaModule)
    },
    { 
        path: "user",
        loadChildren:() => import('./user-login/user-login.module').then(m => m.UserLoginModule)
    },
    { 
        path: "*",
        loadChildren:() => import('./persona/persona.module').then(m => m.PersonaModule)
    },
    
]
@NgModule({
    imports: [RouterModule.forRoot(routes,{ useHash: true })],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }
  