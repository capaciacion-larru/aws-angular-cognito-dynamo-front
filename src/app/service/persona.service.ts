import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

import { Persona } from '../domain/persona';
import { environment } from "../../environments/environment"
@Injectable()
export class PersonaService {
    update(persona: Persona) {
        return this.http.put(environment.baseUrl+"/persona",persona)
    }
    getPersonas() {
        return this.http.get<Persona[]>(environment.baseUrl+"/persona")
        .pipe(
            map(resul=>{
                resul.map((p)=>{
                    p.dni=p.id
                })
                return resul
            })
            )
    }
    constructor(private http: HttpClient) {}
    create(persona:Persona){
        return this.http.post(environment.baseUrl+"/persona",persona)
    }
}