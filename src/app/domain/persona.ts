export interface Persona{
    id?:string,
    dni?:string,
    nombre?:string,
    foto?:string
}