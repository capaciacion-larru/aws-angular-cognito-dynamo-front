import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Auth ,Amplify} from 'aws-amplify';

import { environment } from '../../../environments/environment';
const serviceToken = 'CognitoIdentityServiceProvider.';
export interface IUser {
  email: string;
  password: string;
  showPassword: boolean;
  code: string;
  name: string;
}

@Injectable({
  providedIn: 'root',
})
export class CognitoService {
  dataToken:string | null = "";
  initNameToken = serviceToken + environment.cognito.userPoolWebClientId+ "."//'CognitoIdentityServiceProvider.sn8a6iuosla634lfs21127p0a.';
  private authenticationSubject: BehaviorSubject<any>;

  constructor() {
    Amplify.configure({
      Auth: environment.cognito,
    });

    this.authenticationSubject = new BehaviorSubject<boolean>(false);
  }

  public signUp(user: IUser): Promise<any> {
    return Auth.signUp({
      username: user.email,
      password: user.password,
    });
  }

  public confirmSignUp(user: IUser): Promise<any> {
    return Auth.confirmSignUp(user.email, user.code);
  }

  public signIn(user: IUser): Promise<any> {
    return Auth.signIn(user.email, user.password)
    .then(() => {
      this.authenticationSubject.next(true);
    });
  }

  public signOut(): Promise<any> {
    return Auth.signOut()
    .then(() => {
      this.authenticationSubject.next(false);
    });
  }

  public isAuthenticated(): Promise<boolean> {
    if (this.authenticationSubject.value) {
      return Promise.resolve(true);
    } else {
      return this.getUser()
      .then((user: any) => {
        if (user) {
          return true;
        } else {
          return false;
        }
      }).catch(() => {
        return false;
      });
    }
  }
  
  public isAllAuthenticated(){
    this.dataToken="";
    const nameToken = this.initNameToken + this.getNameTokenId() + '.idToken';
    this.dataToken = localStorage.getItem(nameToken);
    if(this.dataToken != null){
      return true;
    }else{
      return false;
    }
  }
  getAutorizationToken(){
    this.dataToken="";
    const nameToken = this.initNameToken + this.getNameTokenId() + '.idToken';
    this.dataToken = localStorage.getItem(nameToken);
     console.log("TOKEN" , this.dataToken)
    return localStorage.getItem(nameToken);
  }
  getNameTokenId(){

    let nameTokenid = this.initNameToken + 'LastAuthUser';

   return localStorage.getItem(nameTokenid);

  }
  public getUser(): Promise<any> {
    return Auth.currentUserInfo();
  }

  public updateUser(user: IUser): Promise<any> {
    return Auth.currentUserPoolUser()
    .then((cognitoUser: any) => {
      return Auth.updateUserAttributes(cognitoUser, user);
    });
  }

}