import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserLoginRoutingModule } from './user-login-routing.module';
import { CognitoService } from './service/cognitoService.service';
import { FormsModule } from '@angular/forms';
import { SignInComponent } from './sign-in/sign-in.component';
import { ProfileComponent } from './profile/profile.component';
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import { PasswordModule } from "primeng/password";
@NgModule({
  declarations: [
    SignUpComponent,
    SignInComponent,
    ProfileComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    UserLoginRoutingModule,
    InputTextModule,
    ButtonModule,
    PasswordModule
  ],
  providers:[CognitoService]
})
export class UserLoginModule { }
